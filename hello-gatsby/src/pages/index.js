import React from "react"

import Layout from "../components/layout"
import Pagetile from "../components/pagetile"

import { graphql } from "gatsby"
import SEO from "../components/seo"

export default () => (
<Layout>
    <SEO title="{data.site.siteMetadata.title}" description="{data.site.siteMetadata.description}" />
    <Pagetile headerText="Home" />
    <p>Que mundo.</p>
</Layout>
)

export const query = graphql`
  query {
    site {
      siteMetadata {
        title
        description
      }
    }
  }
`