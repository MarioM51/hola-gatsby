import React from "react"
import Pagetile from "../components/pagetile"
import Layout from "../components/layout"
import { graphql } from "gatsby"

export default ({ data }) => (
  <Layout style={{ color: `teal` }}>
    <Pagetile headerText="About e" />
    <p>Such wow. Very React.</p>
    <footer>{data.site.siteMetadata.lastUpdate}</footer>
  </Layout>
  
)

export const query = graphql`
  query {
    site {
      siteMetadata {
        lastUpdate
      }
    }
  }
`