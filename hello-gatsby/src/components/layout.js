import React from "react" 
import { css } from "@emotion/core"
import { rhythm } from "../utils/typography"
import Container from "./container"

import { useStaticQuery, Link, graphql } from "gatsby"

export default ({ children }) => {
  const data = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
          }
        }
      }
    `
  )

 return (
  <header css={css`
      margin: 0 auto;
      max-width: 700px;
      padding: ${rhythm(2)};
      padding-top: ${rhythm(1.5)};`}>
    <Link to="/" style={{ textShadow: `none`, backgroundImage: `none` }}>
      <h1 style={{ display: `inline` }}>
        {data.site.siteMetadata.title}
      </h1>
    </Link>
    <nav css={css`float: right;`}>
      <Link to="/">Home</Link>&nbsp;|&nbsp;
      <Link to="/about">About</Link>&nbsp;|&nbsp;
      <Link to="/lista-usuarios">lista usuarios</Link>&nbsp;|&nbsp;
      <Link to="/my-files">Archivos</Link>&nbsp;|&nbsp;
      <Link to="/blogs">Blogs</Link>&nbsp;|&nbsp;
    </nav>
    <Container>
      {children}
    </Container>
  </header>
  )
 }